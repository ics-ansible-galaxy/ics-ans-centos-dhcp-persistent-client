import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_centos(host):
    assert "PERSISTENT_DHCLIENT=yes" in host.file("/etc/sysconfig/network-scripts/ifcfg-eth0").content_string
